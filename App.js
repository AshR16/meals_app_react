// // import React from 'react'
// // import {View,Text,StyleSheet} from "react-native"

// // export default function App() {
// //   return (
// //     <View style={styles.screen}>
// //       <Text>Hello</Text>
// //     </View>
// //   )
// // }

// // const styles = StyleSheet.create({
// //   screen: {
// //     flex: 1,
// //     alignItems: 'center',
// //     justifyContent: 'center',
// //     backgroundColor:"yellow"
// //   },
// // });

// import React, { useState } from 'react';
// import { Text, View } from 'react-native';
// import * as Font from 'expo-font';
// import { AppLoading } from 'expo';

// // import MealsNavigator from './Navigation/MealsNavigator';

// const fetchFonts = () => {
//   return Font.loadAsync({
//     'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
//     'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
//   });
// };

// export default function App() {
//   const [fontLoaded, setFontLoaded] = useState(false);

//   if (!fontLoaded) {
//     return (
//       <AppLoading
//       startAsync={fetchFonts}
//       onFinish={() => setFontLoaded(true)}
//       onError={(err) => console.log(err)}
//   />
//     );
//   }

//   // <View style={styles.screen}>Hello</View>
//   // return (<MealsNavigator />);
//   return(
//     <View>Hello</View>
//   )
// }

// const styles = StyleSheet.create({
//   screen: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'center',
//     backgroundColor:"yellow"
//   },
// });

// import React, { useState } from 'react';
// import { Text, View } from 'react-native';
// import * as Font from 'expo-font';
// import { AppLoading } from 'expo';

// import MealsNavigator from './navigation/MealsNavigator';

// const fetchFonts = () => {
//   return Font.loadAsync({
//     'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
//     'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
//   });
// };

// export default function App() {
//   const [fontLoaded, setFontLoaded] = useState(false);

//   if (!fontLoaded) {
//     return (
//       <AppLoading
//         startAsync={fetchFonts}
//         onFinish={() => setFontLoaded(true)}
//       />
//     );
//   }

//   return <MealsNavigator />;
// }



import React,{useState} from 'react';
import { Text, View } from 'react-native';

import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';
import MealsNavigator from './Navigation/MealsNavigator';


const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};


export default function App() {
  const [fontLoaded, setFontLoaded] = useState(false);


  if (!fontLoaded) {
    return (
      <AppLoading
    startAsync={fetchFonts}
    onFinish={() => setFontLoaded(true)}
    onError={(err) => console.log(err)}
      />
    );
  }

return <MealsNavigator />

  // return (
  //   <View>
  //     <Text>Open up App.js to start working on your app!</Text>
  //   </View>
  // );
}