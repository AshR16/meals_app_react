import React from 'react'
import { View } from 'react-native';

export default function MealList() {
    const renderMealItem = itemData => {
        return(
         //  <Text>{itemData.item.title}</Text>
         <MealItem 
         title={itemData.item.title}
         image={itemData.item.imageUrl}
         duration={itemData.item.duration}
         complexity={itemData.item.complexity}
         affordability={itemData.item.affordability} 
         onSelectMeal ={()=>{
           props.navigation.navigate(
             {
               routeName: 'MealDetailScreen',
                 params: {
                   mealId: itemData.item.id
                 }
             }
             )
         }} 
         />
        )
      }

    return (
        (
            <View style={styles.list}> 
             {/* <Text>The category meal screen</Text>
             <Text>{selectedCategory.title}</Text>
             <Button title="Meal Detail Screen" onPress={()=>props.navigation.navigate('MealDetailScreen')}/> */}
            <FlatList data={displayedMeals} renderItem={renderMealItem} style={{ width: "92%" }}/>
          
            </View>
          )
    )
}


const styles = StyleSheet.create({
    list: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
