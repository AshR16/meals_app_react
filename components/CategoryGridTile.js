import React from 'react'

import {Text,View,TouchableOpacity,StyleSheet,  Platform,
    TouchableNativeFeedback} from "react-native"

export default function CategoryGridTile(props) {
    console.log(props,"props")
    let TouchableCmp =TouchableOpacity

    if (Platform.OS === 'android' && Platform.Version >= 21) {
        TouchableCmp = TouchableNativeFeedback; // ripple effect for android
      }
    return (
        <View style={styles.grid}>
        <TouchableCmp
        // style={styles.grid}
        // style={{flex:1}}
        // onPress={() => props.navigation.navigate("CategoryMeals")}
        onPress={props.onSelect}
      >
        <View style={{...styles.container,...{backgroundColor:props.color}}}>
          <Text style={styles.title}>{props.title}</Text>
          {/* <Text>hello</Text> */}
        </View>
      </TouchableCmp>
      </View>
    )
    
}


const styles = StyleSheet.create({
    grid: {
      flex: 1,
      margin: 15,
      height: 150,
      elevation:5,
    },
    container:{
        flex:1,
        borderRadius:15,
        // shadow for ios
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        // for android 
       /*  elevation:5, */
        padding:15,
        margin:2,
        justifyContent:'flex-end',
        alignItems:'flex-end'

    },
    title:{
        fontFamily:'open-sans-bold',
        fontSize:20,
        textAlign:'right'
    }
  });
  