// Its all about navigation from one page to the another.
// stack navigator

import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Platform } from 'react-native'

import CategoriesScreen from '../screens/CategoriesScreen';
import CategoryMealsScreen from '../screens/CategoryMealsScreen'
import MealDetailScreen from '../screens/MealDetailScreen'
import { createBottomTabNavigator } from 'react-navigation-tabs';

import FavouriteMealsScreen from '../screens/FavouriteMealsScreen';
import { Ionicons } from '@expo/vector-icons';

const defaultStackOptions= {
     
        headerStyle: {
            backgroundColor: Platform.OS === 'android' ? "#F8485E" : ''
            },
        headerTintColor: Platform.OS === 'android' ? 'white' : ""
    }
  

// stack navigator to move around categories, categoryMeals and MealDetailScreen
const MealsNavigator = createStackNavigator({

    Categories: CategoriesScreen,
    CategoryMeals: CategoryMealsScreen,
    MealDetailScreen: MealDetailScreen,

},{
    defaultNavigationOptions:defaultStackOptions
})

// stack navigator to move around the MealDetailScreen and favorite screen

const FavNavigator = createStackNavigator({

  FavoriteScreen: FavouriteMealsScreen,
  MealDetailScreen: MealDetailScreen,

},{
  defaultNavigationOptions:defaultStackOptions
})

// Tab screen for meals and favorite

const MealsFavoriteTabNavigator = createBottomTabNavigator({
     Meals: {
          screen: MealsNavigator,
          navigationOptions: {
             // function to fetch the icon for the project 
            tabBarIcon: tabInfo => {
              return (
                <Ionicons
                  name="ios-restaurant"
                  size={25}
                  color={tabInfo.tintColor}
                />
              );
            }
          }
        },
        Favorites: {
          screen: FavNavigator,
          navigationOptions: {
            /* tabBarLabel: 'Favorites!', */
            tabBarIcon: tabInfo => {
              return (
                <Ionicons name="ios-star" size={25} color={tabInfo.tintColor} />
              );
            }
          }
        }
      },
      {
        tabBarOptions: {
          activeTintColor: "orange"
        }
      
})

export default createAppContainer(MealsFavoriteTabNavigator);


// import { createStackNavigator, createAppContainer } from 'react-navigation';

// import CategoriesScreen from '../screens/CategoriesScreen';
// import CategoryMealsScreen from '../screens/CategoryMealsScreen';
// import MealDetailScreen from '../screens/MealDetailScreen';

// const MealsNavigator = createStackNavigator({
//   Categories: CategoriesScreen,
//   CategoryMeals: {
//     screen: CategoryMealsScreen
//   },
//   MealDetail: MealDetailScreen
// });

// export default createAppContainer(MealsNavigator);