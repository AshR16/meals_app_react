import React from "react"
import { View,Text,StyleSheet,Button} from "react-native"
import { MEALS } from "../data/dummy-data";


import { HeaderButtons,Item } from "react-navigation-header-buttons";
import { HeaderButton } from "../components/HeaderButton"


export default function MealDetailScreen(props) {
 // console.log(props,"mealsProps")
 const mealId = props.navigation.getParam('mealId')
 //console.log(mealId,"mealId")
 const selectedMeal = MEALS.find(meal => meal.id === mealId);
 //console.log(selectedMeal,"selectedMeal")
    return (
      <View style={styles.screen}> 
       {/* <Text>The Meals Detail screen</Text> */}
       <Text>{selectedMeal.title}</Text>        
       <Button
        title="Go Back to Categories"
        onPress={() => {
          props.navigation.popToTop();
        }}/>

      </View>
    )
}

// Retrieves the data dynamically 
// Used headerButton package to get the header component


MealDetailScreen.navigationOptions = navigationData => {
  const mealId = navigationData.navigation.getParam('mealId');
  const selectedMeal = MEALS.find(meal => meal.id === mealId);
  return {
    headerTitle: selectedMeal.title,
   /*  headerRight: <Text>Hello</Text> */
    
    headerRight: ()=>(
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Favorite"
          // iconName="ios-star"
          name="ios-star"
          onPress={() => {
            console.log('Mark as favorite!');
          }}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
    screen: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });