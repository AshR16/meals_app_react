import React from "react";
import CategoryGridTile from "../components/CategoryGridTile";
import {
  View,
  Text,
  StyleSheet,
  Button,
  FlatList,
  TouchableOpacity,
} from "react-native";

import { CATEGORIES } from "../data/dummy-data";

export default function CategoriesScreen(props) {
  //console.log(props)

  const renderGridItem = (Item) => {
    return (
      <CategoryGridTile title={Item.item.title} color={Item.item.color} onSelect={()=>{
        {
          props.navigation.navigate({
            routeName: 'CategoryMeals',
            params: {
              categoryId: Item.item.id
            }
          });
        }
      }}/>
    )
  };

  return (
    // <View style={styles.screen}>
    //  <Text>The categories screen</Text>
    //  <Button title="Category Meals Screen" onPress={()=>props.navigation.navigate('CategoryMeals')}/>
    // </View>
    // console.log(categoriesData)

    <FlatList
      // keyExtractor={(item, index) => item.id}
      data={CATEGORIES}
      renderItem={renderGridItem}
      numColumns={2}
    />
  );
}

CategoriesScreen.navigationOptions = {
  headerTitle: "Meal Categories",
  headerStyle: {
    backgroundColor: Platform.OS === "android" ? "purple" : "",
  },
  headerTintColor: Platform.OS === "android" ? "white" : "black",
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
 
});

//   import React from 'react';
// import { View, Text, StyleSheet } from 'react-native';

// const CategoriesScreen = props => {
//   return (
//     <View style={styles.screen}>
//       <Text>The Categories Screen!</Text>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   screen: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center'
//   }
// });

// export default CategoriesScreen;
