import React from "react"
import { View,Text,StyleSheet,Button,FlatList } from "react-native"
import MealItem from "../components/MealItem";

import { CATEGORIES,MEALS } from '../data/dummy-data';

export default function CategoryMealsScreen(props) {
 // Get the details of the meal

 const renderMealItem = itemData => {
   return(
    //  <Text>{itemData.item.title}</Text>
    <MealItem 
    title={itemData.item.title}
    image={itemData.item.imageUrl}
    duration={itemData.item.duration}
    complexity={itemData.item.complexity}
    affordability={itemData.item.affordability} 
    onSelectMeal ={()=>{
      props.navigation.navigate(
        {
          routeName: 'MealDetailScreen',
            params: {
              mealId: itemData.item.id
            }
        }
        )
    }} 
    />
   )
 }


  const catId = props.navigation.getParam('categoryId');
  // const selectedCategory = CATEGORIES.find(cat => cat.id === catId);
  const displayedMeals = MEALS.filter(
    meal => meal.categoryIds.indexOf(catId) >= 0
  );  
  
  return (
      <View style={styles.screen}> 
       {/* <Text>The category meal screen</Text>
       <Text>{selectedCategory.title}</Text>
       <Button title="Meal Detail Screen" onPress={()=>props.navigation.navigate('MealDetailScreen')}/> */}
      <FlatList data={displayedMeals} renderItem={renderMealItem} style={{ width: "92%" }}/>
    
      </View>
    )
}

CategoryMealsScreen.navigationOptions = navigationData => {
  //console.log(navigationData)
  const catId = navigationData.navigation.getParam('categoryId');

  const selectedCategory = CATEGORIES.find(cat => cat.id === catId);

  return {
    headerTitle: selectedCategory.title,
    
  }
}


const styles = StyleSheet.create({
    screen: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  });